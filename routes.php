<?php

use Illuminate\Routing\Router;

/** @var $router Router */

// les routes pour la page d'accueil
$router->get('/', 'HomeController@index');
$router->get('/logout', 'HomeController@logout');

// les routes pour le panier
$router->get('/cart', 'CartController@index');
$router->post('/cart/add', 'CartController@store');

// les routes pour la commande
$router->get('/order', 'OrderController@create');
$router->post('/order/add', 'OrderController@store');

// les routes pour les produits
$router->get('/product/{id}', 'ProductController@show');

// catch-all route
$router->any('{any}', function () {
    return view('errors.404');
})->where('any', '(.*)');
