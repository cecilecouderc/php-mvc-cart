<?php

use Illuminate\Session\Middleware\StartSession;

namespace App\Controllers;

use App\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Services\Cart;

/**
 * Controller pour l'home
 */
class HomeController extends Controller {
	/**
	 * Affiche la page du magasin avec tous les produits
	 * @return view retourne la vue avec tous les produits
	 */
	public function index(){
		// return "Something Wrong ! Check your HomeController";
		return view('home', ['products' => Product::all()]);
	}

	/**
	 * Destroy la session php
	 * @return view redirige vers la view home
	 */
	public function logout(){

	}
}
