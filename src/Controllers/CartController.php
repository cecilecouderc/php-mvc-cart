<?php 

namespace App\Controllers;

use Illuminate\Routing\Redirector;
use App\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Services\Cart;

/**
 * Controller pour gérer les commandes et le panier
 */
class CartController extends Controller {

	/**
	 * affiche le panier de l'utilisateur
	 * @param  Cart  $cart Objet pour le panier utilisateur
	 * @return  view retourne la vue order.index
	 */
	public function index(){
		 
		$panier = [];
		foreach($_SESSION['cart'] as $id => $quantity)
		{
			$p = Product::find($id);

			$panier[] = [
				'id' =>$id,
				'quantity' => $quantity,
				'name' => $p->name,
				'price' => $p->price
			];
		}
			return view('cart.index', [
			'cart' => $panier
		]);
	}

	/**
	 * Ajoute un produit au panier 
	 * Fait le compte de tous les produits
	 * Calcule le total
	 * @param  Request $request Récupère les requêtes du client
	 * 
	 * @return view  redirige vers la route principale
	 *				 pour la redirect:
	 * 				 $redirect->to("[routeName]"); redirige vers une route
	 * 				 $redirect->back(); redirige vers la route précédente
	 */
	public function store(Request $request, Redirector $redirect){
		$id = $request->input('id');
		$quantity = $request->input('quantity');
		// echo "id" . $id . " " .  "quantity" . $quantity;
		
		Cart::add($id, $quantity);
		 
		return $redirect->to("/");
	}
}
