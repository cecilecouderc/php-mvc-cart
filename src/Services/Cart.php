<?php

namespace App\Services;

class Cart
{
	/**
	 * Retourne le tableau produits ajouté par l'utilisateur
	 * @return array Data Object
	 */
	public static function get(){
		return [];
	}

	/**
	 * Ajoute un produit dans le panier
	 */
	public static function add($id, $quantity){
		
		$_SESSION["cart"][$id] += $quantity; //tableau associatif "panier" dont l'index est l'id et dans lequel j'ajoute la quantité 

	}

	/**
	 * Compte le nombre d'articles qu'il y a dans le panier
	 */
	public function count(){}

	/**
	 * Calcule le prix total du panier
	 */
	public function total(){}
}