<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Model pour la table customers
 */
class Customer extends Model
{
	/**
	 * Les propriéter éditable de la table customers
	 * @var array
	 */
	protected $fillable = ['first_name', 'last_name', 'address', 'postcode', 'phone'];
	
	/**
	 * désactive le timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * OPTIONAL
	 * Les propriétés éditables de la table users
	 * @var array
	 * @Tips : https://laravel.com/docs/5.8/eloquent-relationships#defining-relationships
	 */
	public function orders(){
	}
}