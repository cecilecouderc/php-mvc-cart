<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Model pour la table orders
 */
class Order extends Model
{
	/**
	 * Les propriétés éditables de la table orders
	 * @var array
	 */
	protected $fillable = ['amount', 'shipped'];

	/**
	 * désactive le timestamps
	 * @var boolean
	 */
	public $timestamps = false;
	/**
	 * OPTIONAL
	 * Les propriétés éditables de la table orders
	 * @var array
	 * @TIP : https://laravel.com/docs/5.8/eloquent-relationships#defining-relationships
	 */
	public function customer(){
	}

	public function products(){
	}

}