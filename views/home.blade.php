@extends('layouts')

@section('content')
<section class="container">
	<h1 class="title">Liste des produits</h1>
	<hr>
	{{-- Boucle pour récupérer des produits (https://laravel.com/docs/5.8/blade), 
					 Bulma : https://bulma.io/documentation/columns/responsiveness/,
									 https://bulma.io/documentation/components/card/
			 --}}

	<div class="columns is-desktop">
		@foreach ($products as $product)
		<div class="column">
			<div class="card">
				<div class="card-image">
					<figure class="image is-4by3">
						<img src="{{$product->picture}}" alt="{{$product->picture}}">
					</figure>
				</div>
				<div class="card-content">
					<div class="media">
						<div class="media-left">
						</div>
						<div class="media-content">
							<p class="title is-4"> {{ $product->name }} </p>
						</div>
					</div>
					<div class="content"> {{$product->price}} €
						<br>
					</div>
				</div>
				<div class="buttons are-medium">
				<a href="/product/{{$product->id}}"><button class="button">En savoir plus</button></a>
				</div>
			</div>
		</div>
		@endforeach
	</div>
<textarea name="" id="" cols="30" rows="30"><?= print_r($_SESSION["cart"])?></textarea>
</section>
@endsection