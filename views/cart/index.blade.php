@extends('layouts')

@section('content')
<section class="container">
	<h1 class="title">Mon panier</h1>
	<hr>
	<table class="table is-striped is-narrow is-hoverable is-fullwidth">
		<thead>
			<tr>
				<th>Articles</th>
				<th>Prix</th>
				<th>Quantité</th>
			</tr>
		</thead>
		<tbody>
			{{-- Blade : Boucles pour récupérer des produits commander ( https://laravel.com/docs/5.8/blade), 
							 Bulma : https://bulma.io/documentation/elements/table/--}}

				@foreach($cart as $product)
				<tr>
					<?php 
					$total+= $product[price]*$product[quantity];
					$totalQuantity+= $product[quantity];
					?>
					<td><p>{{$product[name]}}</p></td>
					<td><p>{{$product[price]}}</p></td>
					<td><p>{{$product[quantity]}}</p></td>
				</tr>
				@endforeach 

		</tbody>
		<tfoot>
			<tr>
				<th>Articles</th>
				<th>Prix</th>
				<th>Quantité</th>
				<th>
					{{-- Afficher le prix total de tous les produits --}}
					<h3 class="subtitle is-5">Total (€) : {{$total}} &euro;</h3>
					{{-- afficher le nombre de produits au total --}}
					<h3 class="subtitle is-5">Nombre de produits :{{$totalQuantity}}</h3>
					<div class="buttons">
						<a href="/" class="button is-small is-default">Retour au shopping</a>
						<a href="/order" class="button is-small is-success">Valider la commande</a>
					</div>
				</th>
			</tr>
		</tfoot>
	</table>
</section>
<textarea name="" id="" cols="30" rows="30"><?= print_r($_SESSION["cart"])?></textarea>
@endsection