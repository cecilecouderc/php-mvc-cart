<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Learn MVC</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="apple-touch-icon" href="apple-touch-icon.png">
		<link rel="stylesheet" href="/css/bulma.min.css"/>
		<link rel="stylesheet" href="/css/all.css"/>
	</head>
	<body>
		<nav class="navbar is-light" role="navigation," aria-label="main navigation">
			<div class="container">
				<div class="navbar-brand">
					<a class="navbar-item" href="#">Ma super Boutique</a>
				</div>
				<div class="navbar-start">
					<a href="#" class="navbar-item">Produits</a>
				</div>
				<div class="navbar-end">
					<span class="navbar-item">
						<a class="button" href="/cart">
							<span class="icon">
								<i class="fas fa-shopping-basket"></i>
							</span>&nbsp; Mon panier : ???&euro;
						</a>
					</span>
					<a href="/logout" class="navbar-item"><span class="button is-danger">Logout</span></a>
				</div>
			</div>
		</nav>

		@yield('content')

    <details>
        <h2>$_SESSION</h2>
        <pre><code>{{var_dump($_SESSION)}}</code></pre>
        <h2>$_COOKIE</h2>
        <pre><code>{{var_dump($_COOKIE)}}</code></pre>
        <h2>session id</h2>
        <pre><code>{{session_id()}}</code></pre>
    </details>
	</body>
</html>