@extends('layouts')

@section('content')
<section class="container">

	<p>{{$products->name}}</p>

	<img src="{{$products->picture}}" alt="{{$product->picture}}">

	<p>{{$products->price}} €</p>

	<form action="/cart/add" method="post">
		<input type="hidden" name="id" value="{{$products->id}}">
		<input class="input" type="number" name="quantity" placeholder="choisir une quantité" min="0" require>
		<button type="submit">Ajouter au panier</button>
	</form>

	{{--
			Affiche toutes les informations d'un seul produit
			avec un bouton pour ajouter les produits et on peut choisir la quantité
		--}}

</section>
@endsection

