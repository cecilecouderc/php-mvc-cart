<?php

use Illuminate\Routing\UrlGenerator;
use Illuminate\Container\Container;
use Illuminate\Routing\Redirector;
use Illuminate\Events\Dispatcher;
use Illuminate\Routing\Router;
use Illuminate\Http\Request;
// Créer le service container
$container = new Container;

//Crée une demande à partir de variables de serveur et la lie au container; 
$request = Request::capture();
$container->instance('Illuminate\Http\Request', $request);

// Utilisation de l'Event Dispatcher
$events = new Dispatcher($container);

// Crée une nouvelle instance de Router
$router = new Router($events, $container);

// Crée les routes groupe sur le namespace global
$router->group(['namespace' => 'App\Controllers'], function (Router $router) {
	// Charge toutes les routes
	require_once '../routes.php';
});

// Envoie les requêtes via le routeur
$response = $router->dispatch($request);

//Envoi les réponses au navigateur
$response->send();



