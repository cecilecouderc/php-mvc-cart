<?php
//on démarre les sessions
// doc : http://php.net/manual/fr/session.examples.basic.php
session_start();

//autoloader de composer qui charge également nos classes
require_once __DIR__.'/vendor/autoload.php'; 
// function utlitaire
require __DIR__.'/functions.php';
// chargement d'eloquent.
require __DIR__.'/config/database.php';
// config pour les routes.
require __DIR__.'/config/router.php';