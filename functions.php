<?php 
use duncan3dc\Laravel\Blade as BladeInstance;
/**
 * Génère les vues avec Blade
 * @param  [String] $path chemin pour la views
 * @param  array  $vars variables passé à la vue
 * @TIPS = https://laravel.com/docs/5.8/blade#introduction
 */
function view($path, $vars = []){
	$tpl = new BladeInstance(__DIR__."/views");
	return $tpl->render($path, $vars);
}

/**
 * Affiche des messages stockés dans la session pour des futurs
 */
function flash(){

}

